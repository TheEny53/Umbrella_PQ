# Paxy

This project started as two different ideas in the context of the topic &#39;Smart City&#39; at the
Blackforest Hackathon 2019.
One idea was tracking the capacity of different venues by counting different MAC-addresses using a raspberry pi's WiFi-Module.
The other one was developing an application to inform citizens and tourists about the
capacity and current occupation of venues and queues, as well as providing an all-in-one platform for reservations, marketing and planning.
As the first idea basically describes the technology which is needed to provide data
for the platform, the idea for project collaboration came up.

Knowing about the origin of the idea eases the understanding of the architecture of
the repository.

This repository acts as umbrella repository, containing all project repositories as Git submodules.
## PROBE FRAMES – TRACKING
This part provides the connection with/to the raspberry pi and implements the task
which is executed.
Wi-Fi devices are continually emitting &quot;probe frames,&quot; calling out for nearby Wi-Fi
networks to connect to. Probe frames can be used to track the MAC address of
nearby devices. With this knowledge we can create a map of the customers nearby
and track how many people are in one place and how long they stay there. We use
a mesh network of several mini computers such as the well-known “Raspberry Pi”
or “Arduino Nano” with these we can cover a large area very cost-effective and
obtain a lot of data about the behavior of the customers in real time. We analyze
this data to show in the APP how busy the locations are and help business
operators to optimize their workflows but more about this topic later in part II and
III. Why does a Wi-Fi device send probe frames? Wi-Fi access points (APs) emit
packets called beacon frames which contain information about the access point
they are advertising. This information allows nearby devices to be aware that the
access point exists and to decide whether or not the network has been connected
to before. If the network is recognized by a device, the device will attempt to
connect to the network.

## Client-Side Data Processor

To avoid large amounts of network traffic and to comply with GDPR-induced regulations, MAC-addresses are being aggregated into a count value to indicate the amount of people on the client-side. Only this value is transmitted to the backend systems for further processing, resulting in no personal data stored on our servers.

Repository name: csv_count

### Technology
* Python

## Platform Backend
To provide consumers and commercial users with a platform to centralize their occupational data, a backend system is implemented.
It provides the following functionalities:
* Registering user accounts for commercial and private usage
* Registering new venues (commercial users only)
* Registering new tracking devices to report occupation data to the backend system (commercial users only)
* Adding opening hours for venue (commercial users only)
* Logging past occupational data (occupation, reservations and timestamps) in an hourly interval (commercial users only)
* Setting up automated promotions that trigger on a pre-set minimum occupation, time and date (commercial users only)
* Providing customers with promotions for reserving a table (commercial users only)
* Reserving tables for a given time and venue, if applicable (private users only)
* Finding nearby venues based on current occupation (private users only)
* Saving favorites (private users only)

Repository name: people_queue

### Technologies:

* Django (Backend)
* PostgreSQL (Main Database)
* Redis (Cache and Worker Queue)
* Heroku (Platform Hosting)

## Frontend
In this part of the repository the frontend of the application is implemented. It uses
the js-framework tabris.js, which enables developing native mobile applications.

Repository name: people-queue-app

## Disclaimer

The intention of this repository is developing a proof of concept(getting the right data
from raspberry and using it) and giving an example for the usage of the invented
technology (application). The application is provided as-is.

## Contributors:
Anika Schmidt,
Enzo Hilzinger,
Felix Ruby,
Christian Renner

All rights reserved.